int nsprites = 6;
int time = -1;
int drawtype;
String[] date = new String[6];
color pencolor;
int size, steps, turn;
int count = 0;
int circleLimiter;
boolean offscreen = false;
Sprite[] Sprites;  
void setup(){
  size(700,700);
  ellipseMode(CENTER);
  rectMode(CORNER);
  circleLimiter = (width > height) ? height : width;
  resetStage();
  smooth();
  newSpriteSet();
  //for (int i = 0; i < nsprites; i++){
  //  Sprites[i] = new Sprite(i);
  //}
}

void newSpriteSet(){
  nsprites = int(random(3,12));
  Sprites = new Sprite[nsprites];
  if(boolean(nsprites%2)){
    drawtype = int(random(1,3));
  } else {
      drawtype = int(random(1,4));
  }
  for (int i = 0; i < nsprites; i++){
    Sprites[i] = new Sprite(i);
  }
}

void draw(){
  if(millis() > time){
    time = millis()+1100;
    pencolor = color(random(255),random(255),random(255));
    size = int(2);
    steps = int(random(5,7));
    turn = int(random(-20,20));
    count++;
    if(count == 30){
      count = 0;
      resetStage();
    }
  }
  for (Sprite sprite : Sprites) {
    sprite.update(turn);
  }
  if (offscreen) {
    for (Sprite sprite : Sprites) {
      sprite.reset();
    }
    offscreen = false;
    count++;
  }
  stroke(255);
  noFill();
  strokeWeight(12);
  ellipse(width/2,height/2,circleLimiter+6,circleLimiter+6);
}

class Sprite {
  float xpos, ypos, x2, y2, tempx, tempy, angle;
  int id;
  Sprite(int num){
    id = num;
    this.reset();
  }
  
  void reset(){
    angle = (360 / nsprites) * id;    
    x2 = xpos = width/2;
    y2 = ypos = height/2;
  }  
  
  void update(float turn){
    angle += turn;
    //angle = (boolean(id%2)) ? angle+turn : angle-turn;
    tempx = steps*cos(radians(angle));
    tempy = steps*sin(radians(angle));
    for (float i = 0; i < 1; i+=0.1){
      noStroke();
      fill(pencolor);
      x2 = xpos+(tempx*i);
      y2 = ypos+(tempy*i);
      if(dist(x2,y2,width/2,height/2) > (circleLimiter/2)){
        offscreen = true;
      } else {
        this.drawMode(drawtype);
      }
    }
    xpos += tempx;
    ypos += tempy;
  }
  void drawMode(float type){
    if(type == 1){
      drawRibbon();
    } else if (type == 2){
      drawLine();
    } else if (type == 3){
      if(boolean(id%2)){
        drawRibbon();
      } else {
        drawLine();
      }
    }
  }
  
  void drawLine(){
    ellipse(x2,y2,size,size);
  }
  
  void drawRibbon(){
    fill(0);
    rect(x2,y2,2,4);
    rect(x2+7,y2,2,4);
    fill(pencolor);
    rect(x2+2,y2,5,4);
  }
}

void resetStage(){
  background(255);
  fill(140);
  noStroke();
  ellipse(width/2,height/2,circleLimiter,circleLimiter);
  newSpriteSet();
}

void mouseReleased(){
  resetStage();
  for (Sprite sprite : Sprites) {
      sprite.reset();
    }
}

void keyPressed(){
  date[0] = nf(year());
  date[1] = nf(month());
  date[2] = nf(day());
  date[3] = nf(hour());
  date[4] = nf(minute());
  date[5] = nf(second());
  if (key == ' '){
    save(join(date,"-")+".png");
  }
}