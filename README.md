A simple kaleidoscope like program.

The screen will clear itself after a little while, or you can click to start over.

Press any key to take a screen capture.
